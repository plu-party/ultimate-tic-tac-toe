package main;

import java.util.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
public class Player {

    private static int OPPONENT = 2;
    private static int PLAYER = 1;

    public static void main(String args[]) {
        Random random = new Random();
        Scanner in = new Scanner(System.in);
        int[][] board = new int[9][9];

        int turnCounter = 1;

        // game loop
        while (true) {
            String move = doNextMove(in, random, board, turnCounter == 1 ? 955 : 100);
            System.out.println(move);
            turnCounter++;
        }
    }

    public static String doNextMove(Scanner in, Random random, int[][] board, long timeBudgetMS) {
        int opponentRow = in.nextInt();
        int opponentCol = in.nextInt();

        //Update the board with the opponents move
        if (opponentCol != -1 && opponentRow != -1) {
            board[opponentRow][opponentCol] = OPPONENT;
        }

        //Retrieve valid actions given as input
        int validActionCount = in.nextInt();
        List<String> validActions = new ArrayList<>(validActionCount);
        for (int i = 0; i < validActionCount; i++) {
            int row = in.nextInt();
            int col = in.nextInt();

            validActions.add(createAction(row, col));
        }

        //Go through all actions and see if the next one is a winner
        String move;

        //Randomly play each action to the end and count wins

        int playouts = 0;
        long startTimeNs = System.nanoTime();

        Map<String, Integer> actionWins = new HashMap<>();
        int i = 0;
        long runningTime = 0;
        while (runningTime < timeBudgetMS) {
            String action = validActions.get(i);
            actionWins.putIfAbsent(action, 0);
            int[][] nextBoard = createStateFromAction(PLAYER, action, board);
            int winner = playToEnd(OPPONENT, action, nextBoard, random);

            if (winner == PLAYER) {
                int numWins = actionWins.get(action);
                actionWins.put(action, numWins + 1);
            }

            runningTime = (System.nanoTime() - startTimeNs) / 1000000;
            i = (i + 1) % validActions.size();
            playouts++;
        }

        System.err.println("number of playouts: " + playouts);
        Map.Entry<String, Integer> maximumWinsAction = null;
        for (Map.Entry<String, Integer> entry : actionWins.entrySet()) {
            if (maximumWinsAction == null || entry.getValue() > maximumWinsAction.getValue()) {
                maximumWinsAction = entry;
            }
        }

        move = maximumWinsAction.getKey();

        // TODO: DRY this up
        int[] actions = parseActionFromString(move);
        int actionRow = actions[0];
        int actionCol = actions[1];
        board[actionRow][actionCol] = PLAYER;

        System.err.println("overbudget by ms: " + (((System.nanoTime() - startTimeNs) / 1000000.0) - timeBudgetMS));
        return move;
    }

    public static int playToEnd(int nextPlayer, String prevAction, int[][] board, Random random) {
        Integer[][] memo = new Integer[3][3];
        //is this game already done?
        int gameResult = get9x9Winner(board, memo);
        while (gameResult == 0) {
            //pick a random action
            List<String> validActions = getValidActions(board, prevAction, memo);
            int nextActionOffset = random.nextInt(validActions.size());

            String action = validActions.get(nextActionOffset);
            int[] actions = parseActionFromString(action);
            int actionRow = actions[0];
            int actionCol = actions[1];
            memo[actionRow / 3][actionCol / 3] = null;


            board[actionRow][actionCol] = nextPlayer;

            nextPlayer = nextPlayer == PLAYER ? OPPONENT : PLAYER;
            gameResult = get9x9Winner(board, memo);
        }
        return gameResult;
    }

    private static String createAction(int row, int col) {
        return row + " " + col;
    }

    public static int[][] createStateFromAction(int player, String action, int[][] board) {
        if (action == null || board == null) {
            throw new RuntimeException("null arguments not allowed");
        }

        int[] actions = parseActionFromString(action);
        if (actions.length != 2) {
            throw new RuntimeException("incorrect format for action, expecting two space separated ints, got: " + action);
        }

        int actionRow;
        int actionCol;
        try {
            actionRow = actions[0];
            actionCol = actions[1];
        } catch (NumberFormatException nfe) {
            throw new RuntimeException("Error while parsing supplied integers", nfe);
        }

        if (board[actionRow][actionCol] != 0) {
            throw new RuntimeException("board is already populated at cell: " + action);
        }

        int[][] clone = new int[board.length][board[0].length];
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[0].length; col++) {
                clone[row][col] = board[row][col];
            }
        }

        clone[actionRow][actionCol] = player;
        return clone;
    }

    /**
     * Returns the player that won for the given board, or 0 if the game is still in progress, or -1 if it's a tie
     */
    public static int get3x3Winner(int[][] board, int originR, int originC) {
        for (int i = 0; i < 3; i++) {
            // if there's a winner at column c + i
            if (board[originR + 0][originC + i] != 0
                    && board[originR + 0][originC + i] == board[originR + 1][originC + i]
                    && board[originR + 0][originC + i] == board[originR + 2][originC + i])
                return board[originR + 0][originC + i];
            // if there's a winner at row r + i
            if (board[originR + i][originC + 0] != 0
                    && board[originR + i][originC + 0] == board[originR + i][originC + 1]
                    && board[originR + i][originC + 0] == board[originR + i][originC + 2])
                return board[originR + i][originC + 0];
        }

        // if there's a winner in the left-to-right (top-to-bottom)
        if (board[originR + 0][originC + 0] != 0 && board[originR + 0][originC + 0] == board[originR + 1][originC + 1] && board[originR + 0][originC + 0] == board[originR + 2][originC + 2])
            return board[originR + 0][originC + 0];
        // if there's a winner from right-to-left  (top-to-bottom)
        if (board[originR + 2][originC + 0] != 0 && board[originR + 2][originC + 0] == board[originR + 1][originC + 1] && board[originR + 2][originC + 0] == board[originR + 0][originC + 2])
            return board[originR + 2][originC + 0];

        // since there's no winner, check to see if there are any empty spaces left
        for (int ri = 0; ri < 3; ri++) {
            for (int ci = 0; ci < 3; ci++) {
                if (board[originR + ri][originC + ci] == 0)
                    return 0; // 0 means the game is still in progress
            }
        }

        return -1; // the game is a tie https://english.stackexchange.com/questions/155621/why-is-a-tie-in-tic-tac-toe-called-a-cats-game
    }

    // collapse the outcome of each 3x3 board into a smaller 3x3 board, then determine the winner of that
    private static int[][] collapse9x9Board(int[][] board, Integer[][] memo) {
        int[][] collapsed = new int[3][3];
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                int originR = r * 3;
                int originC = c * 3;
                int outcome;
                if (memo[r][c] == null) {
                    outcome = get3x3Winner(board, originR, originC);
                    memo[r][c] = outcome;
                } else {
                    outcome = memo[r][c];
                }

                collapsed[r][c] = outcome;
            }
        }

        return collapsed;
    }

    public static int get9x9Winner(int[][] board, Integer[][] memo) {
        int[][] collapsed = collapse9x9Board(board, memo);
        int winner = get3x3Winner(collapsed, 0, 0);

        // in the event of a tie, the winner is whoever won the most 3x3 boards
        if (winner == -1) {
            // keep track of individual wins in case of a tie on the 9x9 board
            int p1Wins = 0;
            int p2Wins = 0;
            for (int r = 0; r < 3; r++) {
                for (int c = 0; c < 3; c++) {
                    int outcome = collapsed[r][c];
                    if (outcome == PLAYER)
                        p1Wins++;
                    else if (outcome == OPPONENT)
                        p2Wins++;
                }
            }
            if (p1Wins > p2Wins) return PLAYER;
            if (p2Wins > p1Wins) return OPPONENT;
            return -1;
        }

        // either someone won (1 or 2), or the game is still in progress (0)
        return winner;
    }

    /**
     * For the given board and previously played action, returns all actions that can be played against the board.
     */
    public static List<String> getValidActions(int[][] board, String previousAction, Integer[][] memo) {
        int[] actions = parseActionFromString(previousAction);
        int previousActionRow9x9 = actions[0];
        int previousActionCol9x9 = actions[1];

        List<String> validActions = new ArrayList<>();


        //"The James" Pseduo-code

        //Create a 2D array of the collapsed board 3x3, stores won/catsgame/inprogress
        int[][] collapsedBoard = collapse9x9Board(board, memo);

        //Check to see if the game is won by the last move - return an empty list
        if (get3x3Winner(collapsedBoard, 0, 0) > 0) {
            //game is won
            return validActions;
        }

        //figure out if it's valid to play everywhere
        int targetSmallBoardRow3x3cell = previousActionRow9x9 % 3;
        int targetSmallBoardCol3x3cell = previousActionCol9x9 % 3;
        //is it the first move?
        boolean isFirstAction = previousActionRow9x9 < 0 || previousActionCol9x9 < 0;
        //is it the first action or check if the target board is not playable
        boolean isValidToPlayEverywhere = isFirstAction || collapsedBoard[targetSmallBoardRow3x3cell][targetSmallBoardCol3x3cell] != 0;

        //go through full 9x9
        //TODO: even though it's more nested for loops, we are acutally iterating the board more than we need to, this could be optimized
        for (int r9x9 = 0; r9x9 < 9; r9x9++) {
            for (int c9x9 = 0; c9x9 < 9; c9x9++) {
                int r3x3cell = r9x9 / 3; //3x3 so we can look at the collapsed board
                int c3x3cell = c9x9 / 3;

                //for each square, look at the status of the small board
                boolean smallBoardIsInProgress = collapsedBoard[r3x3cell][c3x3cell] == 0;
                boolean cellIsEmpty = board[r9x9][c9x9] == 0;
                boolean isInTargetBoard = r3x3cell == targetSmallBoardRow3x3cell && c3x3cell == targetSmallBoardCol3x3cell;
                //if the small box is InProgress && empty add it to the valid actions
                if (smallBoardIsInProgress && cellIsEmpty && (isValidToPlayEverywhere || isInTargetBoard)) {
                    validActions.add(createAction(r9x9, c9x9));
                }
            }
        }

        return validActions;
    }

    /**
     * Action format,
     * min value or col -1, max is 8
     * return format [0] = row, [1] = col
     */
    public static int[] parseActionFromString(String action) {
        //will be bigger then 3 if there is negatives
        //otherwise the action will be length 3
        if (action.length() > 3)
            return new int[]{-1, -1};
        int[] actions = new int[2];
        actions[0] = Character.getNumericValue(action.charAt(0));
        actions[1] = action.charAt(2) - '0';
        return actions;

    }
}

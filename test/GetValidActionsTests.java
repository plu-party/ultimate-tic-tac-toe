import main.Player;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class GetValidActionsTests {
    Integer[][] emptyMemo(){
        return new Integer[3][3];
    }

    @Test
    void firstMove() {
        int[][] board = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
        };

        List<String> actual = Player.getValidActions(board, "-1 -1", emptyMemo());
        assertEquals(81, actual.size());
    }

    @Test
    void nextSmallBoardisEmpty() {
        int[][] board = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
        };

        List<String> actual = Player.getValidActions(board, "2 6", emptyMemo());
        assertEquals(9, actual.size());

        Set<String> actuals = new HashSet<>(actual);
        Set<String> expecteds = new HashSet<>(Arrays.asList("6 0", "6 1", "6 2", "7 0", "7 1", "7 2", "8 0", "8 1", "8 2"));

        assertEquals(expecteds, actuals);
    }

    @Test
    void nextSmallBoardAlreadyFull() {
        int[][] board = {
                {0, 0, 0, 0, 0, 0, 1, 2, 1},
                {0, 0, 0, 0, 0, 0, 2, 2, 1},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 2, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
        };

        List<String> actual = Player.getValidActions(board, "3 2", emptyMemo());

        assertEquals(71, actual.size());

        Set<String> actuals = new HashSet<>(actual);
        Set<String> notExpecteds = new HashSet<>(Arrays.asList("0 6", "0 7", "0 8", "1 6", "1 7", "1 8", "2 6", "2 7", "2 8"));

        for (String notExpected : notExpecteds) {
            assertFalse(actuals.contains(notExpected), notExpected);
        }
    }

    @Test
    void nextSmallBoardAlreadyWonAndNotFull() {
        int[][] board = {
                {0, 0, 0, 0, 0, 0, 1, 2, 1},
                {0, 1, 0, 0, 0, 0, 2, 2, 1},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 2, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
        };

        List<String> actual = Player.getValidActions(board, "1 1", emptyMemo());

        assertEquals(61, actual.size());

        Set<String> actuals = new HashSet<>(actual);
        Set<String> notExpecteds = new HashSet<>(Arrays.asList("3 3", "3 4", "3 5", "4 3", "4 4", "4 5", "5 3", "5 4", "5 5"));

        for (String notExpected : notExpecteds) {
            assertFalse(actuals.contains(notExpected), notExpected);
        }
    }

    @Test
    void lastPlayedActionWonGame() {
        int[][] board = {
                {1, 0, 0, 0, 0, 0, 1, 2, 1},
                {0, 1, 0, 0, 0, 0, 2, 2, 1},
                {0, 0, 1, 0, 0, 0, 1, 1, 2},
                {0, 0, 2, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 1},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
        };

        List<String> actual = Player.getValidActions(board, "1 1", emptyMemo());

        assertEquals(0, actual.size());
    }
}
